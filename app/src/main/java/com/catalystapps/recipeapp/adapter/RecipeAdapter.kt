package com.catalystapps.recipeapp.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.catalystapps.recipeapp.databinding.RecipeItemBinding
import com.catalystapps.recipeapp.model.ReceipeModel


class RecipeAdapter(private val items: ArrayList<ReceipeModel>, var listener: AdapterListener) : RecyclerView.Adapter<RecipeAdapter.ViewHolder>() {
    //this adapter is used to show recipe list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RecipeItemBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.binding.cardView.setOnClickListener(View.OnClickListener {
            listener.onClickedItem(position)
        })
    }

    inner class ViewHolder(val binding: RecipeItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ReceipeModel) {
            binding.recipe = item
            binding.executePendingBindings()
        }
    }
    interface AdapterListener {
        fun onClickedItem(position: Int)
    }

}