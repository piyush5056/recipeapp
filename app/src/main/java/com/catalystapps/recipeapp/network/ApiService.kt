package com.catalystapps.kotlinlearning.network

import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("api")
    fun getReceipe(@Query("p") page: String): Single<ResponseBody>
}