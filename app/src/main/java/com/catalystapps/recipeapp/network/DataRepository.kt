package com.catalystapps.recipeapp.network
import com.catalystapps.kotlinlearning.network.ApiClient
import io.reactivex.Single
import okhttp3.ResponseBody

class DataRepository
{
    fun getRecipeData(page:String): Single<ResponseBody> {
        return ApiClient.getInstance().getAPIService()!!.getReceipe(page)
    }
}