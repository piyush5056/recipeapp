package com.catalystapps.recipeapp.utils

import android.content.Context
import android.widget.Toast

class CommUtils{
    companion object{
        fun showToastShort(context : Context,msg:String){
            Toast.makeText(context,msg, Toast.LENGTH_SHORT).show()
        }
    }
}
