package com.catalystapps.recipeapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.catalystapps.recipeapp.viewmodel.BaseViewModel

abstract class BaseActivity <T : ViewDataBinding, V : BaseViewModel> : AppCompatActivity() {
    var viewDataBinding: T? = null
    private var mViewModel: V? = null
    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun bindingVariable(): Int

    /**
     * @return layout resource id
     */
    abstract fun layoutId(): Int

    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun viewModel(): V
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        performDatabinding()
    }

    private fun performDatabinding(){
        viewDataBinding = DataBindingUtil.setContentView(this, layoutId())
        this.mViewModel = if (mViewModel == null) viewModel() else mViewModel
        viewDataBinding!!.setVariable(bindingVariable(), mViewModel)
        viewDataBinding!!.executePendingBindings()
    }

    fun replaceFragment(id: Int, fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(id, fragment)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.commit()
    }


    fun addFragment(id: Int, fragment: Fragment) {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.add(id, fragment)
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.commit()
    }

}
