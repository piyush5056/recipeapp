package com.catalystapps.recipeapp.view


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.catalystapps.recipeapp.BR

import com.catalystapps.recipeapp.R
import com.catalystapps.recipeapp.adapter.RecipeAdapter
import com.catalystapps.recipeapp.databinding.FragmentRecipeListBinding
import com.catalystapps.recipeapp.network.DataRepository
import com.catalystapps.recipeapp.utils.CommUtils
import com.catalystapps.recipeapp.viewmodel.MainViewModel
import com.catalystapps.recipeapp.viewmodel.ViewModelFactory
import android.app.ProgressDialog
import com.catalystapps.recipeapp.model.ReceipeModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


/**
 * A simple [Fragment] subclass.
 */
class RecipeListFragment : BaseFragment<FragmentRecipeListBinding, MainViewModel>(),
    RecipeAdapter.AdapterListener {


    override fun onClickedItem(position: Int) {
        //going to detail page on item clicked
        var recipeDetailFragment=RecipeDetailFragment(recipeList.get(position).thumbnail,
            recipeList.get(position).ingredients,
            recipeList.get(position).title)
        addFragment(R.id.main_container,recipeDetailFragment)

    }

    lateinit var ntwrkViewModel: MainViewModel
    var fragmentRecipeListBinding: FragmentRecipeListBinding? = null

    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.fragment_recipe_list

    override fun viewModel(): MainViewModel {
        val factory = ViewModelFactory(DataRepository())
        ntwrkViewModel= ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        return ntwrkViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentRecipeListBinding = viewDataBinding as FragmentRecipeListBinding
        fragmentRecipeListBinding!!.viewModel=ntwrkViewModel

        // Pagination
        fragmentRecipeListBinding!!.rvReciepe.addOnScrollListener(recyclerViewOnScrollListener)
        layoutManager= LinearLayoutManager(requireContext())
        fragmentRecipeListBinding!!.rvReciepe.setLayoutManager(layoutManager);

        //loading api first time
        loadRecipeData()
    }


    var progressBar: ProgressDialog? = null
    var recipeList=ArrayList<ReceipeModel>()
    var pagecount:Int=1
    var PAGE_SIZE:Int=0
    var layoutManager:LinearLayoutManager?=null


    fun loadRecipeData(){

        progressBar= ProgressDialog(requireContext())
        progressBar!!.setCancelable(false)
        progressBar!!.setMessage("Loading...")
        progressBar!!.show()
        //calling api to load data
        ntwrkViewModel.loadData(pagecount.toString()).observe(viewLifecycleOwner, Observer {
            if (it.size!=0){
                Log.d("LIST_SIZE", it.size.toString())
                progressBar!!.hide()
                recipeList.addAll(it)
                PAGE_SIZE=it.size
                if (pagecount>1){
                    //if loading on scroll that time only notifydatasetchanged doing
                    fragmentRecipeListBinding!!.rvReciepe.apply {
                        adapter!!.notifyDataSetChanged()}
                }else{
                    //adding adapter
                    fragmentRecipeListBinding!!.rvReciepe.apply {
                        adapter = RecipeAdapter(recipeList,this@RecipeListFragment)}
                }
                }


        })
        //showing error response
        ntwrkViewModel.errorResponse.observe(viewLifecycleOwner, Observer {
            if(!it.equals("")){
                progressBar!!.hide()
                CommUtils.showToastShort(requireContext(),it)
            }
        })
    }


    private val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            //on scroll change
            val visibleItemCount = layoutManager!!.findLastVisibleItemPosition()
            //this will check page size and if on scrolling if we are on last item, it will call api for next pagecount
            if (visibleItemCount==(pagecount*PAGE_SIZE)-1) {
                    pagecount=pagecount+1
                    loadRecipeData()
                }
            }
        }

}
