package com.catalystapps.recipeapp.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.catalystapps.recipeapp.BR

import com.catalystapps.recipeapp.R
import com.catalystapps.recipeapp.databinding.FragmentRecipeDetailBinding
import com.catalystapps.recipeapp.network.DataRepository
import com.catalystapps.recipeapp.viewmodel.MainViewModel
import com.catalystapps.recipeapp.viewmodel.ViewModelFactory
import com.squareup.picasso.Picasso

/**
 * A simple [Fragment] subclass.
 */
class RecipeDetailFragment(
    thumbnail: String,
    ingredients: String,
    title: String
) : BaseFragment<FragmentRecipeDetailBinding, MainViewModel>() {
    lateinit var ntwrkViewModel: MainViewModel
    var fragmentRecipeDetailBinding: FragmentRecipeDetailBinding? = null

    var titleDetail:String?=null
    var ingredientsDetail:String?=null
    var thumbnailDetail:String?=null

    init {
        titleDetail=title
        ingredientsDetail=ingredients
        thumbnailDetail=thumbnail
    }


    override val bindingVariable: Int
        get() = BR.viewModel

    override val layoutId: Int
        get() = R.layout.fragment_recipe_detail

    override fun viewModel(): MainViewModel {
        val factory = ViewModelFactory(DataRepository())
        ntwrkViewModel= ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        return ntwrkViewModel
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragmentRecipeDetailBinding = viewDataBinding as FragmentRecipeDetailBinding
        //showing data
        fragmentRecipeDetailBinding!!.tvtitle.setText(titleDetail)
        fragmentRecipeDetailBinding!!.tvIngredients.setText(ingredientsDetail)
        Picasso.get().load(thumbnailDetail).into(fragmentRecipeDetailBinding!!.imageView)
    }

}
