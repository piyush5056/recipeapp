package com.catalystapps.recipeapp.view

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.catalystapps.recipeapp.BR
import com.catalystapps.recipeapp.R
import com.catalystapps.recipeapp.databinding.ActivityMainBinding
import com.catalystapps.recipeapp.network.DataRepository
import com.catalystapps.recipeapp.viewmodel.MainViewModel
import com.catalystapps.recipeapp.viewmodel.ViewModelFactory

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    override fun bindingVariable(): Int {
        return BR.viewModel

    }

    private var factory: ViewModelFactory? = null

    private lateinit var mainViewModel: MainViewModel
    private var binding: ActivityMainBinding? = null

    override fun layoutId(): Int {
        return R.layout.activity_main
    }

    override fun viewModel(): MainViewModel {
        factory = ViewModelFactory(DataRepository())
        mainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)
        return mainViewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = viewDataBinding as ActivityMainBinding
        var recipeListFragment=RecipeListFragment()
        //add the recipe list fragment to activity
        addFragment(R.id.main_container,recipeListFragment)
    }

    override fun onBackPressed() {
        var fragment=supportFragmentManager.findFragmentById(R.id.main_container)
        if (fragment!=null){
            if (fragment is RecipeListFragment){
                finish()
            }else{
                super.onBackPressed()
            }
        }else{
            super.onBackPressed()

        }
    }
}
