package com.catalystapps.recipeapp.model

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Picasso

data class ReceipeModel(var title:String,
                        var ingredients:String,
                        var thumbnail:String) {


    companion object{
        @JvmStatic
        @BindingAdapter("bind:image")
        fun loadImage(view: ImageView, url: String) {
            if (!(url.equals("")||url== null)){
                Picasso.get().load(url).into(view)
            }
        }
    }
}