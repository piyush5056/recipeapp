package com.catalystapps.recipeapp.viewmodel
import androidx.lifecycle.MutableLiveData
import com.catalystapps.recipeapp.model.ReceipeModel
import com.catalystapps.recipeapp.network.DataRepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import org.json.JSONObject
import java.util.ArrayList

class MainViewModel(dataRepository: DataRepository): BaseViewModel(dataRepository) {
    var recipeList=MutableLiveData<ArrayList<ReceipeModel>>()
    var errorResponse=MutableLiveData<String>()

    var list=ArrayList<ReceipeModel>()
    //this method will call api and return data fetched from api
    fun loadData(pageCount:String):MutableLiveData<ArrayList<ReceipeModel>>{
        disposable.add(
            getRepository().getRecipeData(pageCount).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(
                    object : DisposableSingleObserver<ResponseBody>() {
                        override fun onSuccess(value: ResponseBody) {
                            var dataResponse=value.string()
                            var jsonObject=JSONObject(dataResponse);
                            var jsonArr=jsonObject.getJSONArray("results")
                            list = Gson().fromJson<ArrayList<ReceipeModel>>(
                                jsonArr.toString(),
                                object :
                                    TypeToken<ArrayList<ReceipeModel>>() {

                                }.type
                            )
                            recipeList.value=list
                        }

                        override fun onError(error: Throwable) {
                            errorResponse.value=error.message
                        }
                    })
        )
        return recipeList
    }
}