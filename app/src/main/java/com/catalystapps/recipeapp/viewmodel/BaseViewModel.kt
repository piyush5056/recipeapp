package com.catalystapps.recipeapp.viewmodel

import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.catalystapps.recipeapp.network.DataRepository
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(private val mRepository: DataRepository) :ViewModel()
{

    val disposable: CompositeDisposable = CompositeDisposable()


    fun getRepository(): DataRepository {
        return this.mRepository
    }

}
